import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

/**
 * @swagger
 * definitions:
 *   Login:
 *    type: object
 *    properties:
 *      email:
 *        type: string
 *      password:
 *        type: string
 *    required:
 *      - email
 *      - password
 */


export default class Login extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
