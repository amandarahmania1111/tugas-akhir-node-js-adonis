import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

/**
 * @swagger
 * definitions:
 *   Verify:
 *    type: object
 *    properties:
 *      otp_code:
 *        type: string
 *      email:
 *        type: string
 *    required:
 *      - otp_code
 *      - email
 */



export default class Verify extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
