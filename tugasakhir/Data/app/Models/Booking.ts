import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Field from 'App/Models/Field'
import User from 'App/Models/User'

/**
 * @swagger
 * definitions:
 *   Booking:
 *    type: object
 *    properties:
 *      title:
 *        type: string
 *      play_date_start:
 *        type: timestamp
 *      play_date_end:
 *        type: timestamp
 *    required:
 *      - title
 *      - play_date_start
 *      - play_date_end
 */

/**
 * @swagger
 * definitions:
 *   Schedule:
 *    type: object
 *    properties:
 *      user_id:
 *        type: integer
 *      field_id:
 *        type: integer
 *    required:
 *      - user_id
 *      - field_id
 */


export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string
 
  @column.dateTime()
  public playDateStart: DateTime

  @column.dateTime()
  public playDateEnd: DateTime

  @column()
  public userId: number

  @column()
  public fieldId: number


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Field)
  public field: BelongsTo<typeof Field>

  @belongsTo(() => User)
  public bookingUser: BelongsTo<typeof User>
//bila nama databasenya custom
  @manyToMany(() => User,{
    pivotTable : 'schedules'
  })
  public players: ManyToMany<typeof User>
}
