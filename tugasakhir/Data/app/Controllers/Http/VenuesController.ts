import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
// import CreateVenueValidator from 'App/Validators/CreateVenueValidator';
import CreateFieldValidator from 'App/Validators/CreateFieldValidator';

import Database from '@ioc:Adonis/Lucid/Database';

// //models
import Venue from 'App/models/Venue'

export default class VenuesController {
    //hanya pakai response
    // public async index({ request, response }: HttpContextContract){
    //     if (request.qs()) {
    //         let name = request.qs().name
    //         let na = await Database.from('venues').where('name', name).select('*')
    //         console.log(request.qs().name)
    //         //return biar yang bawah ga ngikut
    //         return response.status(200).json({ messages: 'success get data venues', data: na})
    //     }
    //     let ven = await Database.from('venues').select('*')
    //     return response.status(200).json({ messages: 'success get data venues', data: ven})
    //     //hasilnya array data yang ada di tabel venues
    // }

    //dengan ORM
    public async index({ request, response }: HttpContextContract){
        if (request.qs().name){
            let name = request.qs().name
            let ven = await Venue.findBy('name', name)
            return response.status(200).json({ message: 'success get data venues', ven})
   }
           let as = await Database.from('venues').select('*')
        return response.status(200).json({ messages: 'success get data venues', data: as})

        }

        
    


    // public async venue({ request, response }: HttpContextContract){
    //     try{
    //         const payLoad = await request.validate(CreateVenueValidator);
            
    //        } catch (error){
    //            response.unprocessableEntity({ errors: error.messages})
    //        }
    // }
    // public async bookings({ request, response }: HttpContextContract){
    //     try{
    //         const pay = await request.validate(CreateFieldValidator);
    //        } catch (error){
    //            response.unprocessableEntity({ errors: error.messages})
    //        }
    // }
    // public async store({request, response }: HttpContextContract) {
    //     try {
    //         await request.validate(CreateFieldValidator);
    //         //await Database.from('venues')
    //         //await Database.table('venues')
    //         //querybuilder
    //         let newVenue = await Database.table('venues').returning('id').insert({
    //             name: request.input('name'),
    //             address: request.input('address'),
    //             phone: request.input('phone'),
    //         })

    //         response.created({ message: 'created!', data: newVenue})    
    //     } catch (error) {
    //         response.unprocessableEntity({error: error.messages})
    //     }
    // }

    public async store({request, response}: HttpContextContract  ){
        const data = await request.validate(CreateFieldValidator)
        let {name , address, phone} = data
        let venue = await Venue.create({
            name,
            address,
            phone
        })
        return response.status(200).json({
            message :'berhasil',
            venue
        })
    }
    
    //agar tidak array pake show diakhir ditambah first
    //first or fail error 404
    // public async show({params, response,}: HttpContextContract) {
    //     let venue = await Database.from('venues').where('id', params.id).select('id', 'name', 'address', 'phone').firstOrFail()
    //     return response.ok({ messages: 'success get data venue with id', data: venue})
    // }

//orm
    public async show({params, response,}: HttpContextContract) {
        let venue = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        return response.ok({ messages: 'success get data venue with id', data: venue})

    }


    //update
    // public async update({request, response, params}: HttpContextContract) {
    //     let id = params.id
    //     let affectedRows = await Database.from('venues').where('id', id).update({
    //         name: request.input('name'),
    //         address: request.input('address'),
    //         phone: request.input('phone'),
    //     })
    //     return response.ok({ messages: 'updated!'})
    // }

    //cara orm
    public async update({request, response, params}: HttpContextContract) {
        let id = params.id
        let venue = await Venue.findOrFail(id)
        venue.name = request.input('name')
        venue.address = request.input('address')
        venue.phone = request.input('phone')
     
        venue.save()
          return response.ok({ messages: 'updated!'})
    }
      
    //delete
    // public async destroy({params, response}: HttpContextContract) {
    //     await Database.from('venues').where('id', params.id).delete()
    //     return response.ok({ messages: 'deleted!' })
    // }
    //return nya cuman 1 karena true false

    //cara ORM
       public async destroy({params, response}: HttpContextContract) {
           let venue = await Venue.findOrFail(params.id)
           await venue.delete()
            return response.ok({ messages: 'deleted!' })
 
       }
 

}

 








