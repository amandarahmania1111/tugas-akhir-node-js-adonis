import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import Booking from 'App/Models/Booking'

//dengan model
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
  // public async index ({request, response}: HttpContextContract) {
  //   if(request.qs()){
  //     let name = request.qs().name
  //     let fieldsFiltered = await Database.from('fields').where('name', name).select('id', 'name', 'type', 'venue_id')
  //     return response.status(200).json({message: 'success get contacts', data: fieldsFiltered })
  //   }
  //   let fields = await Database.from('fields').select('*')
  //   return response.status(200).json({message: 'success get contacts', data: fields })
  // }

  public async index({ request, response }: HttpContextContract){
    if (request.qs().name){
        let name = request.qs().name
        let ven = await Field.findBy('name', name)
        return response.status(200).json({ message: 'success get data fields', ven})
}
       let as = await Database.from('fields').select('*')
    return response.status(200).json({ messages: 'success get data fields', data: as})

    }


  // public async store ({request, response, params}: HttpContextContract) {
  //   console.log(request.body())
  //   try {
  //       await request.validate(CreateVenueValidator)
  //       console.log('setelah validate')
  //       let newFieldId = await Database.table('fields').returning('id').insert({
  //           name: request.input('name'),
  //           type: request.input('type'),
  //           venue_id: params.venue_id
  //       })
  //       response.created({message: 'created', newId: newFieldId})
  //   }catch (error) {
  //       response.unprocessableEntity({error: error.messages})
  //   }
  // }

public async store ({request, response, params}: HttpContextContract) {
  console.log(request.body())
  //try {
      const venue = await Venue.findByOrFail('id', params.venue_id)
      //await request.validate(FieldsValidator)
      console.log('setelah validate')

      //MODEL ORM
      const newField = new Field();
          newField.name = request.input('name')
          newField.type = request.input('type')
          await newField.related('venue').associate(venue)

          //simpan ke db metode .save
          //pake await karena memanggil function sql 
          await newField.save()
          console.log("id: ", newField.id) //true diterminal
          response.created({message: 'created', data: newField})
  //} 
  // catch (error) {
  //     response.unprocessableEntity({error: error.messages})
 
}


  // public async show ({params, response}: HttpContextContract) {
  //     let field = await Database.from('fields').where('id', params.id).select('id', 'name', 'type', 'venue_id').firstOrFail()
  //     return response.status(200).json({message: 'success get fields with id', data: field})
  // }

//   public async show({params, response,}: HttpContextContract) {
//     let field = await Field.find(params.id)
//     return response.ok({ messages: 'success get data venue with id', data: field})

// }

//setelah direlasi
  public async show ({params, response}: HttpContextContract) {
    const field = await Field.query().where('id', params.id).preload('bookings', (bookingQuery)=>{
      bookingQuery.select(['title', 'play_date_start', 'play_date_end']) 
    }).firstOrFail()
    return response.ok({status: 'success', data: field})
  } 





  // public async update ({request, response, params}: HttpContextContract) {
  //     let id = params.id
  //     await Database.from('fields').where('id', id).update({
  //         name: request.input('name'),
  //         type: request.input('type'),
  //         venue_id: params.venue_id
  //     })
  //     return response.status(200).json({message: 'updated'})
  // }

//ORM
public async update({request, response, params}: HttpContextContract) {
  let id = params.id
  let field = await Field.findOrFail(id)
  field.name = request.input('name')
  field.type = request.input('type')
  field.venueId = request.input('venue_id')

  field.save()
    return response.ok({ messages: 'updated!'})
}

  // public async destroy ({response, params}: HttpContextContract) {
  //     let id = params.id
  //     await Database.from('fields').where('id', id).delete()
  //     return response.ok({message: 'deleted'})
  // }

  //ORM
  public async destroy({params, response}: HttpContextContract) {
    let field = await Field.findOrFail(params.id)
    await field.delete()
     return response.ok({ messages: 'deleted!' })

}

}