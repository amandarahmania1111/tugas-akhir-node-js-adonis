import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
// import BookingValidator from 'App/Validators/BookingValidator';
// import TheVenueValidator from 'App/Validators/TheVenueValidator';
import Booking from 'App/Models/Booking';
// import Field from 'App/Models/Field';
import User from 'App/Models/User';
import Database from '@ioc:Adonis/Lucid/Database';

export default class FormsController {

    public async index({ request, response}: HttpContextContract) {
        if(request.qs().title){
            let title = request.qs().title
            let bookingFiltered = await Booking.findBy("title", title)
            return response.status(200).json({message: 'Berhasil get booking', data: bookingFiltered })
        }
        let booking = await Booking.all()
        return response.status(200).json({message: 'Berhasil get booking', data: booking })
    }


    // public async store({request, params, response, auth}: HttpContextContract){
    //     const field = await Field.findByOrFail('id', params.field_id)
    //     const user = auth.user!
    //     const payload = await request.validate(BookingValidator)

    //     const booking = new Booking()
    //     booking.playDateStart = payload.play_date_start
    //     booking.playDateEnd = payload.play_date_end
    //     booking.title = payload.title
    //     booking.related('field').associate(field) 
    //     user.related('myBookings').save(booking)
    //     return response.created({status: 'success', data: booking})
 
    // }
    public async store({request, params, response, auth}: HttpContextContract){
        let user = await User.find(auth.user?.id)


        if (user){
            await user.related('myBookings').create({
                title: request.input('title'),
                playDateStart: request.input('play_date_start'),
                playDateEnd: request.input('play_date_end'),
                fieldId: params.field_id
            })

            return response.json({
                message: 'created booking success'
            })
        }
    }

    // public async join({ response, auth, params}: HttpContextContract){
    //     const booking = await Booking.find(params.id)
    //     let user = auth.user!
    //     await booking.related('players').attach([user.id])
    //     return response.ok({status:'success', data:'successfully join'})

    //     // else (
    //     //     await booking.related('players').detach([user.id])
    //     //     return response.ok({'gagal'})
    
    //     // )

//     /    public async show({params, response }: HttpContextContract) {
//         try {
//         const booking = await Booking.query()
//             .select('id', 'play_date_start', 'play_date_end')
//             .where('id', '=', params.id)
//             .withCount('players', (query) => {
//                 query.as('players_count')
//         })
//             .preload('players', (query) => {
//                 query.select('id', 'name', 'email')
//         })
//             .firstOrFail()
//         const {id, play_date_start, play_date_end, players} = booking.toJSON()
//         const players_count = booking.$extras.players_count
//         response.ok({ message : 'success', data: {id, play_date_start, play_date_end, players_count, players} })
//         } catch (error) {
//         if (error.messages) {
//             return response.unprocessableEntity({ message: 'failed', error: error.messages})
//         } else {
//             return response.unprocessableEntity({ message: 'failed', error: error.message})
//         }
//     }
//     }
// / }


    public async show({ response, params}: HttpContextContract){
        const booking = await Booking.query().where('id', params.id ).preload('players', (userQuery)=>{
            userQuery.select(['name', 'email', 'id'])
        }).withCount('players').firstOrFail()
        let rawData = JSON.stringify(booking)
        let datawithCount = {players_count: booking.$extras.players_count, ...JSON.parse(rawData)}
        return response.ok({status: 'success', data: datawithCount})
    } 
    // public async join({ request, response, auth, params}: HttpContextContract){
    //     const book = schema.create({

    //          user_id : schema.string({}, [
    //              rules.unique
    //          ] )
    //     })
    //     const books = await request.validate({schema: book})
    //     if(books){
    //         let user = await User.find(auth.user?.id)
    //         const booking = await Booking.findOrFail(params.id)
    
    //         await booking.related('players').detach([user!.id])
    //     }{
    //         return response.ok({status:'success', data:'successfully join'})
    //     }   
    // }

    // email : schema.string({}, [
    //     rules.email(),
    //     rules.unique({
    //         table : 'users',
    //         column : 'email'
    //     })
    // ]),


    // public async index({ request, response, params }: HttpContextContract){
    //     let name = request.qs().name
    //     response.status(200).json({ name, b: params.b})
    // }
    // public async venue({ request, response }: HttpContextContract){
    //     try{
    //         const payLoad = await request.validate(TheVenueValidator);
    //         response.created({message: 'registrasi berhasil', data : payLoad})
    //        } catch (error){
    //            response.unprocessableEntity({ errors: error.messages})
    //        }
    // }
    // public async booking({ request, response }: HttpContextContract){
    //     try{
    //         const payLoad = await request.validate(BookingValidator);
    //         response.created({message: 'registrasi berhasil', data : payLoad})
           
    //        } catch (error){
    //            response.unprocessableEntity({ errors: error.messages})
    //        }
    // }
    public async join({ response, auth, params}: HttpContextContract){
        const booking = await Booking.findOrFail(params.id)
        let user = auth.user!
        const abc = await Database.from('schedules').where('booking_id', params.id).where('user_id', user.id).first()

        if(!abc){
            await booking.related('players').attach([user.id]) 
            return response.ok({status:'success', data:'succesfully join'}) 
        }
        else{
            await booking.related('players').detach([user.id])

        }
    }

    public async update({request, response, params}: HttpContextContract) {
        let id = params.id
        let booking = await Booking.findOrFail(id)
        booking.title = request.input('title')
        booking.playDateStart = request.input('play_date_start')
        booking.playDateEnd = request.input('play_date_end')

        booking.fieldId = request.input('field_id')
      
        booking.save()
          return response.ok({ messages: 'updated!'})
      }
      public async destroy({params, response}: HttpContextContract) {
        let booking = await Booking.findOrFail(params.id)
        await booking.delete()
         return response.ok({ messages: 'deleted!' })
    
    }
        

}

 
