import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
    // public async register ({request, response, auth}: HttpContextContract){
    //     try{
    //         const data = await request.validate(UserValidator)
    //         const newUser = await User.create(data)
    //         const userId = auth.user?.id
    //         console.log('user id:', userId)
    //         return response.created({message : 'register', newUser})
    
    //     } catch (error){
    //         return response.unprocessableEntity({messages : error.messages})
    //     }
    // }
    






    public async register({request, response }: HttpContextContract){
        const payload = await request.validate(UserValidator)
        const newUser = await User.create({
            name: payload.name,
            password: payload.password,
            email: payload.email,
            role: payload.role
        })
        let otp_code: number = Math.floor(100000 + Math.random() * 900000)
        await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})
        await Mail.send((message) => {
            message
              .from('admin@sanberdev.com')
              .to(payload.email)
              .subject('selemat datang')
              .htmlView('mail/otp_verification', { name: payload.name, otp_code: otp_code })
          })
      
    
        return response.created({status: 'success', data: newUser, message: 'silahkan kirim kode verifikasi dari email Anda'})
    }
    
    // public async login({request, response, auth}: HttpContextContract){
    //    try {      
    //        const userSchema = schema.create({
    //            email : schema.string(),
    //            password : schema.string()
    //        }) 
    //        await request.validate({schema : userSchema})
    //         const email = request.input('email')
    //    const password = request.input('password')
    //    const token = await auth.use('api').attempt( email , password)
    //    return response.ok({message : 'login success', token})

    //     } catch (error) {
    //         if (error.guard){
    //             return response.badRequest({message: 'login error', error: error.message})         
    //         } else{
    //             console.log(error.messages)
    //             return response.badRequest({message: 'login error', error: error.messages})         

    //         }
    //     }
    // }
    public async login({request, response, auth}: HttpContextContract){
const loginSchema = schema.create({
    email : schema.string({trim : true}),
    password : schema.string({trim:true})
})
const payload = await request.validate({schema: loginSchema})
const token = await auth.use('api').attempt(payload.email, payload.password)
return response.ok({status : 'success', data:token})
}


    public async otp_verification({request, response}: HttpContextContract) {
      const otp_code = request.input('otp_code')
      const email = request.input('email')
      
      const user = await User.findByOrFail('email', email)

      const otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

      if (user?.id == otpCheck.user_id) {
        user.isVerified = true
        await user?.save()
        return response.status(200).json({ messages: 'Berhasil Konfirmasi OTP'})
      } else {
        return response.status(400).json({ message: 'Gagal Verifikasi OTP'})
      }
      //console.log()
    }




}