/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})
// Route.group(()=>{
//   Route.resource('venues', 'VenuesController').apiOnly().middleware({
//     '*': ['auth', 'verify', 'role'],
//   })
// Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
//   '*': ['auth', 'verify'],
// })
// Route.resource('fields.bookings', 'BookingsController').apiOnly().middleware({
//   '*': ['auth', 'verify'],
// })
// //buat  put field controller terpisah karena bookingan ga bisa diupdate

// Route.put('/bookings/:id', 'BookingsController.join').middleware(['auth', 'verify']).as('booking.join')
// // Route.post('bookings', 'BookingsController.bookings').as('forms.booking')
// Route.post('register', 'AuthController.register').as('auth.register')
// Route.post('login', 'AuthController.login').as('auth.login')
// Route.post('otp-verification', 'AuthController.otp_verification').as('auth.verify')

// })
Route.group(()=> {
  Route.group(() => {
   Route.group(() => {
      Route.resource('venues', 'VenuesController').apiOnly()
      Route.resource('venues.fields', 'FieldsController').apiOnly()
    }).middleware('role')

    Route.group(() => {
      // Route.post('booking/:id/join', 'BookingsController.join').as('booking.join')
      // Route.post('fields/:id/booking', 'BookingsController.store').as('booking.store')
      // Route.get('booking/:id', 'BookingsController.show').as('booking.show')
      Route.put('/bookings/:id', 'BookingsController.join').as('booking.join')

      Route.resource('fields.bookings', 'BookingsController').apiOnly()
    })//.middleware('Role:user')
  }).middleware(['auth', 'verify'])

  Route.post('register', 'AuthController.register').as('auth.register')
  Route.post('login', 'AuthController.login').as('auth.login')
  Route.post('otp-confirmation', 'AuthController.otp_verification').as('auth.otp_verification') 
}).prefix('/api/v1')
Route.get('/api/hello', 'TestController.hello')


